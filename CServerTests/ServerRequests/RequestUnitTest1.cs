﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CServerTests.ServerRequests
{
    [TestClass]
    public class RequestUnitTest1
    {
        private const string ADRESS = @"http://localhost:4297";
        private const string PATH = "_TestFolder";

        private static JObject ServerRequest(string botID, string name, string data)
        {
            try
            {
                using (var wb = new WebClient())
                {
                    var j = new NameValueCollection();
                    j["botID"] = botID;
                    j["name"] = name;
                    j["path"] = PATH;
                    j["msg"] = data;

                    var response = wb.UploadValues(ADRESS, "POST", j);
                    string responseInString = Encoding.UTF8.GetString(response);
                    return JObject.Parse(responseInString);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return JObject.Parse(@"{
                      Error: 'Request error-> steamid: " + botID + " name:" + name + @"'
                }");
            }
        }

        [TestMethod]
        public void RequestTest()
        {
            var posData = new Dictionary<string, int>();
            posData.Add("x", 123);
            posData.Add("y", 456);

            var h = ServerRequest("0000", "RequestTest", JsonConvert.SerializeObject(posData));
            if (h.TryGetValue("Error", out JToken value))
                Assert.Fail("Error: " + value);
            try
            {
                var c = h.Value<bool>("Success");
                Assert.IsTrue(c);
            }
            catch (Exception)
            {
                Assert.Fail("Error: didnt received good answer");
            }
        }

        [TestMethod]
        public void RequestNNTest()
        {
            var posData = new Dictionary<string, int>();
            posData.Add("x", 123);
            posData.Add("y", 456);

            var h = ServerRequest("0000", "RequestNNTest", JsonConvert.SerializeObject(posData));
            try
            {
                var succ = h.Value<bool>("Success");
                Assert.IsTrue(succ);
                var data = h.Value<int>("TrainData");
                Assert.AreEqual(1, data);
            }
            catch (Exception)
            {
                Assert.Fail("Error: didnt received good answer");
            }
        }

        [TestMethod]
        public void GetValidBotID()
        {
            var h = ServerRequest("0000", "GetValidBotID", "{}");
            var succ = h.Value<bool>("Success");
            Assert.IsTrue(succ);
            var data = h.Value<string>("BotID");
            Assert.AreEqual("1001", data);
        }

        [TestMethod]
        public void SaveDataTest()
        {
            var posData = new Dictionary<string, int>();
            posData.Add("x", 123);
            posData.Add("y", 456);

            var h = ServerRequest("0000", "SaveDataTest", JsonConvert.SerializeObject(posData));
            var succ = h.Value<bool>("Success");
            Assert.IsTrue(succ);
        }
    }
}
