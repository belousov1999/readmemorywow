var NN = require("./../Class/EvoNN.js");

console.log("EvoNN preloaded");

var nn = new NN(["screenPointX", "screenPointY"]);

var lastID = 1000;


class LogicManager {

	public static TestTrain(): number {
		nn.TrainFromData();
		console.log("Train completed");
		return 1;
	}

	public static GetValidBotID(): string {
		return ++lastID + "";
	}
}

module.exports = LogicManager