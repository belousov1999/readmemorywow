var NN = require("./../Class/EvoNN.js");
console.log("EvoNN preloaded");
var nn = new NN(["screenPointX", "screenPointY"]);
var lastID = 1000;
var LogicManager = /** @class */ (function () {
    function LogicManager() {
    }
    LogicManager.TestTrain = function () {
        nn.TrainFromData();
        console.log("Train completed");
        return 1;
    };
    LogicManager.GetValidBotID = function () {
        return ++lastID + "";
    };
    return LogicManager;
}());
module.exports = LogicManager;
//# sourceMappingURL=LogicManager.js.map