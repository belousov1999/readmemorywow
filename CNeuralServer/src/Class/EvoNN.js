var fs = require('fs');
var brain = require('brain.js/browser');
var EvoNN = /** @class */ (function () {
    function EvoNN(outValues) {
        this.SaveFolderName = "./_SaveData/";
        this.net = new brain.NeuralNetwork();
        this.save_file = this.SaveFolderName + 'NNsave.json';
        this.testData_file = this.SaveFolderName + 'TestData.json';
        this.DataFolder = "./_BotData/";
        this.DataFileName = "data.json";
        this.outputValues = outValues;
        this.Trained = false;
    }
    EvoNN.prototype.TrainNN = function (json_data) {
        this.net.train(json_data, {
            iterations: 10000,
            log: true,
            callbackPeriod: 10,
            logPeriod: 10,
        });
    };
    EvoNN.prototype.Run = function (data) {
        var output = this.net.run(data);
        console.log(JSON.stringify(data) + " --> " + JSON.stringify(output));
        return output;
    };
    EvoNN.prototype.GetFilesData = function () {
        var _this = this;
        var all_data = [];
        fs.readdirSync(this.DataFolder).forEach(function (file) {
            if (file !== _this.DataFileName)
                return;
            var _FileData = JSON.parse(fs.readFileSync(_this.DataFolder + file, 'utf8')); // inner file strings
            all_data.push(_FileData);
        });
        return all_data;
    };
    EvoNN.CalculateMinMax = function (d) {
        var min = [];
        var max = [];
        for (var k in d)
            for (var kk in d[k]) {
                var v = d[k][kk];
                if (min[kk] === undefined) {
                    min[kk] = v;
                    max[kk] = v;
                }
                if (v < min[kk])
                    min[kk] = v;
                if (v > max[kk])
                    max[kk] = v;
            }
        return { "min": min, "max": max };
    };
    EvoNN.ConvertDataToNNRange = function (data) {
        //Find minus minimum value
        var minmax = EvoNN.CalculateMinMax(data);
        //List with only positive values
        var plusData = [];
        for (var k in data)
            for (var kk in data[k]) {
                var vv = data[k][kk];
                if (plusData[k] === undefined)
                    plusData[k] = [];
                if (minmax.min[kk] < 0)
                    plusData[k][kk] = vv + Math.abs(minmax.min[kk]);
                else
                    plusData[k][kk] = vv;
            }
        //Find max value in positive list
        minmax = EvoNN.CalculateMinMax(plusData);
        //Divide positive list by max value
        var rangeData = [];
        for (var k in plusData)
            for (var kk in plusData[k]) {
                var vv = plusData[k][kk];
                if (rangeData[k] === undefined)
                    rangeData[k] = {};
                if (minmax.max[kk] <= 1.0)
                    rangeData[k][kk] = vv;
                else
                    rangeData[k][kk] = vv / minmax.max[kk];
            }
        return rangeData;
    };
    EvoNN.IsOutputKey = function (outValues, key) {
        for (var k in outValues) {
            if (outValues[k] == key)
                return true;
        }
        return false;
    };
    EvoNN.DeconvertOutputValues = function (data, dataToConvert, outputValues) {
        var minmax_minus = EvoNN.CalculateMinMax(data);
        //List with only positive values
        var plusData = [];
        for (var k in data)
            for (var kk in data[k]) {
                var vv = data[k][kk];
                if (plusData[k] === undefined)
                    plusData[k] = [];
                if (minmax_minus.min[kk] < 0)
                    plusData[k][kk] = vv + Math.abs(minmax_minus.min[kk]);
                else
                    plusData[k][kk] = vv;
            }
        //Find max value in positive list
        var minmax_plus = EvoNN.CalculateMinMax(plusData);
        var converted = {};
        for (var k in dataToConvert) {
            var vv = dataToConvert[k];
            if (!EvoNN.IsOutputKey(outputValues, k))
                continue;
            if (minmax_plus.max[k] <= 1.0)
                converted[k] = vv;
            else
                converted[k] = vv * minmax_plus.max[k];
            if (minmax_minus.min[k] < 0)
                converted[k] -= Math.abs(minmax_minus.min[k]);
        }
        return converted;
    };
    EvoNN.ConvertToTrainData = function (data, outputValues) {
        var trainData = [];
        for (var k in data) {
            var inputData = {};
            var outpudData = {};
            for (var kk in data[k]) {
                if (EvoNN.IsOutputKey(outputValues, kk))
                    outpudData[kk] = data[k][kk];
                else
                    inputData[kk] = data[k][kk];
            }
            trainData.push({ input: inputData, output: outpudData });
        }
        return trainData;
    };
    EvoNN.prototype.TrainFromData = function () {
        this.AllData = this.GetFilesData();
        var rangeData = EvoNN.ConvertDataToNNRange(this.AllData[0]);
        fs.writeFile(this.testData_file, JSON.stringify(rangeData), 'utf8', function (err) { if (err !== undefined)
            console.log(err); });
        var converted = EvoNN.ConvertToTrainData(rangeData, this.outputValues);
        fs.writeFile(this.SaveFolderName + "Converted.json", JSON.stringify(converted), 'utf8', function (err) { if (err !== undefined)
            console.log(err); });
        this.TrainNN(converted);
        /*
        var out = this.Run({ "windowSizeX": 1, "windowSizeY": 1, "targetPosX": 1, "targetPosY": 0.9850746268656716, "targetPosZ": 1, "cameraPosX": 0.9962894248608535, "cameraPosY": 1, "cameraPosZ": 0.9714285714285714, "cameraMatrixX1": 0.5199966757583517, "cameraMatrixX2": 0, "cameraMatrixX3": 0.195455659, "cameraMatrixY1": 1, "cameraMatrixY2": 0.5192484160462499, "cameraMatrixY3": 0, "cameraMatrixZ1": 0.5192938481, "cameraMatrixZ2": 0.116755426, "cameraMatrixZ3": 0.92487824 });
        var deconvert = EvoNN.DeconvertOutputValues(this.AllData[0], out, this.outputValues);
        fs.writeFile(this.SaveFolderName + "Deconverted.json", JSON.stringify(deconvert), 'utf8', (err) => { if (err !== undefined) console.log(err); });
        
        return deconvert;
        */
    };
    EvoNN.prototype.SaveTrainData = function (net_data) {
        fs.writeFile(this.save_file, JSON.stringify(net_data), 'utf8', function (err) { });
    };
    EvoNN.prototype.LoadTrainData = function () {
        var json = fs.readFileSync(this.save_file, 'utf8');
        this.net.fromJSON(JSON.parse(json));
    };
    return EvoNN;
}());
module.exports = EvoNN;
//# sourceMappingURL=EvoNN.js.map