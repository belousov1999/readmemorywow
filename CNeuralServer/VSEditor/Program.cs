﻿namespace CNeuralServer
{
    class Program
    {
        const string DEBUG_PATH = @"bin\Debug";

        static void Main(string[] args)
        {
            var dir = System.IO.Directory.GetCurrentDirectory();
            if (dir.Contains(DEBUG_PATH))
                dir = dir.Substring(0, dir.Length - DEBUG_PATH.Length);
            {
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.FileName = dir + "build.bat";
                p.StartInfo = startInfo;
                p.Start();
                p.WaitForExit();
            }
            {
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.FileName = dir + "start.bat";
                p.StartInfo = startInfo;
                p.Start();
            }
        }
    }
}
