﻿
class Bot {
    constructor(botID) {
        this.botID = botID
        this.HeartBeat();
        this.onlinePeriod = 90; //Time in seconds since last heartbeat
    }
    //Updating activity time
    HeartBeat() { this.lastHeartbeat = new Date().getTime(); }

    //Time period from last HeartBeat in seconds
    TimeSinceLastHB() { 
        if (this.lastHeartbeat === undefined) {
            this.HeartBeat();
            return 0;
        }
        return (new Date().getTime() - this.lastHeartbeat) / 1000;
    }

    //Is normal time since last activity
    IsOnline() { return this.TimeSinceLastHB() <= this.onlinePeriod; }
}

module.exports = Bot;
