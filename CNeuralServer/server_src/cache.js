const fs = require("fs"),
	devServer = false;

class DataCache {
	HasEntry(name) {
		return new Promise(accept => fs.exists(`${name}`, accept))
	}

	GetEntry(name) {
		return new Promise((accept, reject) => fs.readFile(`${name}`, (err, data) => {
			if (err) reject(err)
			else accept(data)
		}))
	}

	SetEntry(name, data) {
		return new Promise((accept, reject) => fs.writeFile(`${name}`, data, err => {
			if (err) reject(err)
			else accept()
		}))
	}

	GetEntryKeyList(tableName) {
		return new Promise((accept, reject) => fs.readdir(tableName, (err, files) => {
			if (err) reject(err)
			else accept(files)
		}))
	}
}

module.exports = DataCache;
