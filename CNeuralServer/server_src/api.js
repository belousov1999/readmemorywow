const DataCache = require("./cache"),
	path = require('path'),
	logicManager = require("./../src/Core/LogicManager.js"),
	Bot = require("./Data/Bot.js");

var _Bots = [];
var _ClicksData = [];

function IsNormalBotID(BotID) {
	return BotID != "0000";
}

function IsBotCreated(BotID) {
	for (var b in _Bots)
		if (b.botID == BotID)
			return true;
	return false;
}

function GetBot(BotID) {
	for (var b in _Bots)
		if (b.botID == BotID)
			return b;
}

function AddUniqueDataToFile(filePath, data) {
	try {
		dataCache.HasEntry(filePath).then(exists => {
			if (!exists) {
				var d = []
				d.push(JSON.parse(data));
				dataCache.SetEntry(filePath, JSON.stringify(d));
				return;
			}

			dataCache.GetEntry(filePath).then(fileData => {
				fileData = JSON.parse(fileData);

				for (var d in fileData) {
					var c = 0;
					for (var f_d in data) {
						if (d === f_d && fileData[f_d] === data[f_d])
							c++;
					}
					if (data.length == c)
						return;
				}
				fileData.push(JSON.parse(data));
				dataCache.SetEntry(filePath, JSON.stringify(fileData));
			});
		});
	} catch (e) {
		console.log(`AddUniqueDataToFile: Save file to ${filePath} is failed: ${e}`);
	}
}

var api = {
	/* TEST API CALLS  starts here */
	"RequestTest": (req, res, data) => {
		try {
			console.log(`RequestTest:[${data.botID}]`)
			res.setHeader("Content-Type", "application/javascript")
			res.send({ "Success": true })
		} catch (e) {
			console.log(e);
		}
	},
	"RequestNNTest": (req, res, data) => {
		try {
			console.log(`RequestNNTest:[${data.botID}]`);
			var r = logicManager.TestTrain();
			res.setHeader("Content-Type", "application/javascript");
			res.send({ "Success": true, "TrainData": r });
		} catch (e) {
			console.log(e);
		}
	},
	"SaveDataTest": (req, res, data) => {
		try {
			console.log(`SaveDataTest:[${data.botID}] Data:[${data.msg}]`);
			AddUniqueDataToFile(`${data.path}/data.json`, data.msg);
			res.setHeader("Content-Type", "application/javascript");
			res.send({ "Success": true });
		} catch (e) {
			console.log(e);
		}
	},
	/* TEST API CALLS  ends here */

	//Return unique ID
	"GetValidBotID": (req, res, data) => {
		console.log(`GetValidBotID:[${data.botID}]`)
		var BotID = logicManager.GetValidBotID();
		res.setHeader("Content-Type", "application/javascript")
		res.send({ "Success": true, "BotID": BotID })
	},
	//Send activity info -> online
	"HeartBeat": (req, res, data) => {
		var bot;
		if (IsNormalBotID(data.botID) && !IsBotCreated(data.botID))
			bot = new Bot(data.botID);
		else
			bot = GetBot(data.botID);
		bot.HeartBeat();
		console.log(`HeartBeat:[${data.botID}]`)
		res.setHeader("Content-Type", "application/javascript")
		res.send({ "Success": true })
	},
	"SendClickData": (req, res, data) => {
		console.log(`SendClickData:[${data.botID}]`)
		AddUniqueDataToFile(`${data.path}/data.json`, data.msg);
		res.setHeader("Content-Type", "application/javascript")
		res.send({ "Success": true })
	},
}

function secure(data) {
	if (data.botID.length != 4 && typeof (data.botID) != "string")
		throw "Invalid botID"

	data.path = (data.path || "").replace(/\\/g, "/").replace(/(?:\/)+/g, "/").replace(/(?:\.)+/g, ".")
	return data
}
Object.size = function (obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};
function HandleAPI(req, res) {
	var b
	if (Object.size(req.body) == 1) {
		for (k in req.body) {
			if (k === undefined) {
				res.sendStatus(400)
				return
			}
			if (k.length > 4) {
				b = k
			}
		}
	}
	if (b !== undefined) {
		req.body = JSON.parse(b)
	}

	var data = secure(req.body)
	//console.log("Path = "+data.path)

	if (!data.name || data.name === "") {
		res.sendStatus(400)
		return
	}

	var callback = api[data.name]
	try {
		if (callback)
			callback(req, res, data)
		else
			res.sendStatus(404)
	} catch (e) {
		res.status(503).send(JSON.stringify(e))
	}
}

module.exports = () => {
	dataCache = new DataCache()

	return HandleAPI
}