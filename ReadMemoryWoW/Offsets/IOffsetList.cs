﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadMemoryWoW.Offsets
{
    public interface IOffsetList
    {
        IntPtr DescriptorTargetGuid { get; }

        IntPtr WowObjectDescriptor { get; }

        IntPtr WowObjectGuid { get; }

        IntPtr WowObjectPosition { get; }

        IntPtr WowObjectType { get; }

        IntPtr WowUnitPosition { get; }

        IntPtr DescriptorExp { get; }

        IntPtr DescriptorMaxExp { get; }

        IntPtr DescriptorMaxHealth { get; }

        IntPtr DescriptorMaxMana { get; }

        IntPtr DescriptorMaxRage { get; }

        IntPtr DescriptorMaxRuneenergy { get; }

        IntPtr DescriptorRage { get; }

        IntPtr DescriptorRuneenergy { get; }

        IntPtr ComboPoints { get; }

        IntPtr PlayerRotation { get; }

        IntPtr IsAutoAttacking { get; }

        IntPtr DescriptorUnitFlags { get; }

        IntPtr DescriptorUnitFlagsDynamic { get; }

        IntPtr DescriptorHealth { get; }

        IntPtr DescriptorMana { get; }

        IntPtr DescriptorEnergy { get; }

        IntPtr DescriptorMaxEnergy { get; }

        IntPtr DescriptorLevel { get; }

        IntPtr DescriptorInfoFlags { get; }

        IntPtr CurrentlyCastingSpellId { get; }

        IntPtr CurrentlyChannelingSpellId { get; }

        IntPtr IsWorldLoaded { get; }

        IntPtr PlayerGuid { get; }

        IntPtr TargetGuid { get; }

        IntPtr ClientConnection { get; }

        IntPtr CurrentObjectManager { get; }

        IntPtr FirstObject { get; }

        IntPtr NextObject { get; }

        IntPtr MapId { get; }

        IntPtr ZoneId { get; }

        IntPtr NameStore { get; }

        IntPtr NameString { get; }

        IntPtr NameBase { get; }

        IntPtr NameMask { get; }

        IntPtr WowGameobjectType { get; }

        IntPtr WowGameobjectLevel { get; }

        IntPtr WowGameobjectDisplayId { get; }

        IntPtr WowGameobjectPosition { get; }

        IntPtr ClickToMoveX { get; }

        IntPtr ClickToMoveY { get; }

        IntPtr ClickToMoveZ { get; }

        IntPtr ClickToMoveAction { get; }

        IntPtr ClickToMoveDistance { get; }

        IntPtr ClickToMoveGuid { get; }

        IntPtr ClickToMovePendingMovement { get; }

        IntPtr ClickToMovePointer { get; }

        IntPtr ClickToMoveTurnSpeed { get; }

        IntPtr CursorType { get; }
    }
}
