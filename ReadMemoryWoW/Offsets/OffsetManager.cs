﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadMemoryWoW.Offsets
{
    public static class OffsetManager
    {
        public static IOffsetList OffsetList { get; private set; }
        public static void Initialise()
        {
            OffsetList = new Offsets_12340();
        }
    }
}
