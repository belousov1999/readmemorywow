﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadMemoryWoW.Offsets
{
    public class Offsets_12340 : IOffsetList
    {
        public IntPtr DescriptorTargetGuid { get; } = new IntPtr(0x48);

        public IntPtr WowObjectDescriptor { get; } = new IntPtr(0x8);

        public IntPtr WowObjectGuid { get; } = new IntPtr(0x30);

        public IntPtr WowObjectPosition { get; } = new IntPtr(0xE8);

        public IntPtr WowObjectType { get; } = new IntPtr(0x14);

        public IntPtr WowUnitPosition { get; } = new IntPtr(0x798);

        public IntPtr DescriptorExp { get; } = new IntPtr(0x1E3C);

        public IntPtr DescriptorMaxExp { get; } = new IntPtr(0x1E40);

        public IntPtr DescriptorMaxHealth { get; } = new IntPtr(0x80);

        public IntPtr DescriptorMaxMana { get; } = new IntPtr(0x84);

        public IntPtr DescriptorMaxRage { get; } = new IntPtr(0x88);

        public IntPtr DescriptorMaxRuneenergy { get; } = new IntPtr(0x9C);

        public IntPtr DescriptorRage { get; } = new IntPtr(0x68);

        public IntPtr DescriptorRuneenergy { get; } = new IntPtr(0x7C);

        public IntPtr ComboPoints { get; } = new IntPtr(0xBD084D);

        public IntPtr PlayerRotation { get; } = new IntPtr(0x7A8);

        public IntPtr IsAutoAttacking { get; } = new IntPtr(0xA20);

        public IntPtr DescriptorUnitFlags { get; } = new IntPtr(0xEC);

        public IntPtr DescriptorUnitFlagsDynamic { get; } = new IntPtr(0x13C);

        public IntPtr DescriptorHealth { get; } = new IntPtr(0x60);

        public IntPtr DescriptorMana { get; } = new IntPtr(0x64);

        public IntPtr DescriptorEnergy { get; } = new IntPtr(0x70);

        public IntPtr DescriptorMaxEnergy { get; } = new IntPtr(0x90);

        public IntPtr DescriptorLevel { get; } = new IntPtr(0xD8);

        public IntPtr DescriptorInfoFlags { get; } = new IntPtr(0x5C);

        public IntPtr CurrentlyCastingSpellId { get; } = new IntPtr(0xA6C);

        public IntPtr CurrentlyChannelingSpellId { get; } = new IntPtr(0xA80);

        public IntPtr IsWorldLoaded { get; } = new IntPtr(0xBEBA40);

        public IntPtr PlayerGuid { get; } = new IntPtr(0xCA1238);

        public IntPtr TargetGuid { get; } = new IntPtr(0xBD07B0);

        public IntPtr ClientConnection { get; } = new IntPtr(0xC79CE0);

        public IntPtr CurrentObjectManager { get; } = new IntPtr(0x2ED0);

        public IntPtr FirstObject { get; } = new IntPtr(0xAC);

        public IntPtr NextObject { get; } = new IntPtr(0x3C);

        public IntPtr MapId { get; } = new IntPtr(0xAB63BC);

        public IntPtr ZoneId { get; } = new IntPtr(0xAF4E48);

        public IntPtr NameStore { get; } = new IntPtr(0xC5D940);

        public IntPtr NameString { get; } = new IntPtr(0x20);

        public IntPtr NameBase { get; } = new IntPtr(0x1C);

        public IntPtr NameMask { get; } = new IntPtr(0x24);

        public IntPtr WowGameobjectType { get; } = new IntPtr(0x54);

        public IntPtr WowGameobjectLevel { get; } = new IntPtr(0x58);

        public IntPtr WowGameobjectDisplayId { get; } = new IntPtr(0x20);

        public IntPtr WowGameobjectPosition { get; } = new IntPtr(0x1D8);

        public IntPtr ClickToMoveX { get; } = new IntPtr(0xCA11D8 + 0x8C);

        public IntPtr ClickToMoveY { get; } = new IntPtr(0xCA11D8 + 0x90);

        public IntPtr ClickToMoveZ { get; } = new IntPtr(0xCA11D8 + 0x94);

        public IntPtr ClickToMoveAction { get; } = new IntPtr(0xCA11D8 + 0x1C);

        public IntPtr ClickToMoveDistance { get; } = new IntPtr(0xCA11D8 + 0xC);

        public IntPtr ClickToMoveGuid { get; } = new IntPtr(0xCA11D8 + 0x20);

        public IntPtr ClickToMovePendingMovement { get; } = new IntPtr(0xCA1200);

        public IntPtr ClickToMovePointer { get; } = new IntPtr(0xBD08F4);

        public IntPtr ClickToMoveTurnSpeed { get; } = new IntPtr(0xCA11D8 + 0x4);

        public IntPtr CursorType { get; } = new IntPtr(0xC26DE8);
    }
}
