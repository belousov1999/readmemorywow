﻿using Newtonsoft.Json;
using ReadMemoryWoW.Class;
using ReadMemoryWoW.Managers.GameData;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReadMemoryWoW.Core;

namespace ReadMemoryWoW.UI.Actions
{
    public static class CreatePoint
    {
        private static PositionPoint LastPoint;
        private static bool LastPointCreated = false;
        private const string FOLDER_PATH = "SaveData", JSON_PATH = FOLDER_PATH+"/Points.json";

        public static void SavePoint(PositionPoint point)
        {
            LastPoint = point;
            LastPointCreated = true;
            UpdateFile();
        }

        public static void DeleteLastPoint()
        {
            if (!LastPointCreated)
                return;
            LastPointCreated = false;
            UpdateFile();
        }

        private static void UpdateFile()
        {
            if (!StatesData.IsPointsInited && File.Exists(JSON_PATH))
            {
                string file = File.ReadAllText(JSON_PATH);
                StatesData.InitPoints(file);
                StatesData.PointsList.Add(LastPoint);
            }
            
            var pos_json = JsonConvert.SerializeObject(StatesData.PointsList);
            if (!Directory.Exists(FOLDER_PATH))
                Directory.CreateDirectory(FOLDER_PATH);
            File.WriteAllText(JSON_PATH, pos_json);
        }

    }
}
