﻿using Newtonsoft.Json;
using ReadMemoryWoW.Class;
using ReadMemoryWoW.Core;
using ReadMemoryWoW.Managers.GameData;
using ReadMemoryWoW.Managers.GameData.Storage;
using ReadMemoryWoW.Managers.Server;
using ReadMemoryWoW.MemoryUtil;
using ReadMemoryWoW.Offsets;
using ReadMemoryWoW.UI.Actions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReadMemoryWoW.UI.Wnds
{
    public partial class Form1 : Form
    {
        private StateManager stateManager = null;
        private ObjectManager objMgr;
        public static bool BotInited = false, BotStarted = false;

        private const string BOT_DISABLED = "DISABLED", BOT_ENABLED = "ENABLED";

        public Form1()
        {
            InitializeComponent();
            InitBot();
        }

        public void InitBot()
        {
            if (BotInited)
                return;

            var xmemory = new XMemory();
            var success = xmemory.Attach("Wow");
            Console.WriteLine("Success: " + success);
            if (!success)
            {
                Console.ReadKey();
                return;
            }

            ServerBotData.Init();
            label2.Text = "BotID: " + ServerBotData.BotID;

            OffsetManager.Initialise();


            objMgr = new ObjectManager(xmemory, new InMemoryBotCache("cache.bin"));
            objMgr.UpdateWowObjects();

            stateManager = new StateManager();
            BotInited = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BotStarted = !BotStarted;
            label1.Text = BotStarted ? BOT_ENABLED : BOT_DISABLED;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ServerBotData.HeartBeat();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            XMemory.Instance.Read(OffsetManager.OffsetList.CursorType, out int cursorType);
            Console.WriteLine("CursorType: "+ cursorType);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CreatePoint.SavePoint(new PositionPoint(objMgr.Player.Position, objMgr.MapId, objMgr.ZoneId));
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                if (BotInited && BotStarted)
                    stateManager.Update();
                objMgr.UpdateWowObjects();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Error in BasementEntry: " + ex + "\n");
                Console.ResetColor();
            }
        }
    }
}
