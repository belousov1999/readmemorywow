﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadMemoryWoW.Class
{
    public struct PositionPoint
    {
        public Vector3 position;
        public int mapID;
        public int zoneID;

        public PositionPoint(Vector3 position, int mapID, int zoneID)
        {
            this.position = position;
            this.mapID = mapID;
            this.zoneID = zoneID;
        }
    }
}
