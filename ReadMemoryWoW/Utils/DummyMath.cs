﻿using ReadMemoryWoW.Managers.GameData.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadMemoryWoW.Utils
{
    public static class DummyMath
    {
        #region CONSTS
        public const float DoublePI = (float)Math.PI * 2.0f;
        const float top_barrier = (float)Math.PI * 2.0f + 2f;
        #endregion

        #region METHODS
        public static float GetFacingAngle(Player player, Class.Vector3 targetPosition)
        {
            float angle = (float)Math.Atan2(targetPosition.Y - player.Position.Y, targetPosition.X - player.Position.X);
            if (angle < 0.0f)
            {
                angle += (float)Math.PI * 2.0f;
            }
            else if (angle > (float)Math.PI * 2)
            {
                angle -= (float)Math.PI * 2.0f;
            }
            Console.WriteLine("Angle_1: "+angle);
            angle -= player.Rotation;
            angle = Math.Abs(angle);
            Console.WriteLine("Angle_3: " + angle);
            return angle;
        }

        public static bool IsFacing(Player player, Class.Vector3 targetPosition)
        {
            var angle = GetFacingAngle(player, targetPosition);
            return IsFacing(angle);
        }

        public static bool IsFacing(float angle) => (Math.Abs(angle) > 0f && Math.Abs(angle) < 0.9f) || (angle > DoublePI + 0f && angle < DoublePI + 0.9f);

        
        #endregion
    }
}
