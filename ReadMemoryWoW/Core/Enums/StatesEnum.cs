﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadMemoryWoW.Core.Enums
{
    public enum StatesEnum
    {
        None = -1,
        Idle = 0,
        MovingToPoint = 1,
        MovingToTarget = 2,
        AttackingTarget = 3,
        Healing = 4
    }
}
