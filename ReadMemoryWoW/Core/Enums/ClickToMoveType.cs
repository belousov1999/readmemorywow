﻿namespace ReadMemoryWoW.Core.Enums
{
    public enum ClickToMoveType : int
    {
        None = 0,//-
        FaceTarget = 1,//For facing
        FaceDestination = 2,//-
        Stop = 3,//Stop
        Move = 4, //Moving
        Interact = 5,//-
        Loot = 6,//-
        InteractObject = 7,//-
        FaceOther = 8,//-
        Skin = 9, //-
        AttackPos = 10, //-
        AttackGuid = 11, //attacking
        Attack = 16, //-
        WalkAndRotate = 19 //-
    }
}
