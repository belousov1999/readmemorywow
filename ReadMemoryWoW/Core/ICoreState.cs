﻿using ReadMemoryWoW.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadMemoryWoW.Core
{
    public interface ICoreState
    {
        /// <summary>
        /// State identifier
        /// </summary>
        StatesEnum eState { get; } 

        /// <summary>
        /// Invoking after programm start 
        /// </summary>
        void Start();

        /// <summary>
        /// Invoking every time period
        /// </summary>
        void Update();
    }
}
