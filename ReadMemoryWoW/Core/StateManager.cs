﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReadMemoryWoW.Core.Enums;
using ReadMemoryWoW.Core.States;

namespace ReadMemoryWoW.Core
{
    public class StateManager : State
    {
        public override StatesEnum eState { get; } = StatesEnum.Idle;

        public static Dictionary<StatesEnum, ICoreState> StatesDict = new Dictionary<StatesEnum, ICoreState>();

        public static StateManager Instance = null;

        public static StatesEnum eCurrentState { get; private set; } = StatesEnum.Idle;
        public static StatesEnum ePrevState { get; private set; } = StatesEnum.None;

        public StateManager()
        {
            Start();
        }

        public override void Start()
        {
            if (Instance != null)
                return;
            Instance = this;
            StatesDict.Add(StatesEnum.Idle, this);
            StatesDict.Add(StatesEnum.MovingToPoint, new MovementPointState());
            StatesDict.Add(StatesEnum.MovingToTarget, new MovementTargetState());
            StatesDict.Add(StatesEnum.AttackingTarget, new AttackingTargetState());

            ExecuteState(StatesEnum.MovingToPoint);
            foreach (var kv in StatesDict)
                kv.Value.Start();
        }

        public override void Update()
        {
            var State = StatesDict.First(x => x.Key == eCurrentState).Value;
            if (State != this)
            {
                State.Update();
                return;
            }
        }

        public static void ExecuteState(StatesEnum newState)
        {
            Console.WriteLine("Change current state: " + eCurrentState + " ==> " + newState);
            ePrevState = eCurrentState;
            eCurrentState = newState;
            if (UI.Wnds.Form1.BotStarted)
                Instance.Update();
        }
    }
}
