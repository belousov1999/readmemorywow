﻿using System;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Threading;
using ReadMemoryWoW.Core.Enums;
using ReadMemoryWoW.MemoryUtil;
using ReadMemoryWoW.Offsets;

namespace ReadMemoryWoW.Core.Actions
{
    public static class MovingOptions
    {
        #region Import
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool WriteProcessMemory(IntPtr processHandle, IntPtr baseAddress, IntPtr buffer, int size, out IntPtr numberOfBytesWritten);
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int wMsg, IntPtr wParam, IntPtr lParam);
        #endregion

        #region KeyCommands
        public static void SendKey(IntPtr vKey, int minDelay = 20, int maxDelay = 40)
        {
            const uint KEYDOWN = 0x100;
            const uint KEYUP = 0x101;

            IntPtr windowHandle = XMemory.GetMainWindowHandle();

            // 0x20 = Spacebar (VK_SPACE)
            SendMessage(windowHandle, (int)KEYDOWN, vKey, new IntPtr(0));
            Thread.Sleep(new Random().Next(minDelay, maxDelay)); // make it look more human-like :^)
            SendMessage(windowHandle, (int)KEYUP, vKey, new IntPtr(0));
            Thread.Sleep(new Random().Next(minDelay / 4, maxDelay / 4));
        }

        public static void StartSendingKey(IntPtr vKey, int minDelay = 20, int maxDelay = 40)
        {
            const uint KEYDOWN = 0x100;

            IntPtr windowHandle = XMemory.GetMainWindowHandle();

            // 0x20 = Spacebar (VK_SPACE)
            SendMessage(windowHandle, (int)KEYDOWN, vKey, new IntPtr(0));
            Thread.Sleep(new Random().Next(minDelay, maxDelay));
        }

        public static void StopSendingKey(IntPtr vKey, int minDelay = 20, int maxDelay = 40)
        {
            const uint KEYUP = 0x101;

            IntPtr windowHandle = XMemory.GetMainWindowHandle();

            SendMessage(windowHandle, (int)KEYUP, vKey, new IntPtr(0));
            Thread.Sleep(new Random().Next(minDelay, maxDelay));
        }

        
        public static void SendForward() => SendKey(new IntPtr((int)System.Windows.Forms.Keys.Up), 2300, 2400);

        public static void StartSendingForward() => StartSendingKey(new IntPtr((int)System.Windows.Forms.Keys.Up), 100, 300);

        public static void StopSendingForward() => StopSendingKey(new IntPtr((int)System.Windows.Forms.Keys.Up), 100, 300);

        public static void SendBackward() => SendKey(new IntPtr((int)System.Windows.Forms.Keys.Down), 300, 400);

        public static void SendLeft() => SendKey(new IntPtr((int)System.Windows.Forms.Keys.Left), 300, 400);

        public static void SendRight() => SendKey(new IntPtr((int)System.Windows.Forms.Keys.Right), 300, 400);
        #endregion

        #region InternalCommands
        public static void Face(Class.Vector3 pos, ulong guid, float turnSpeed = 4.28f, float distance = 3.0f) => SendCommand(ClickToMoveType.FaceTarget, pos, guid, turnSpeed, distance);

        public static void Stop(Class.Vector3 pos, ulong guid, float turnSpeed = 6.28f, float distance = 3.0f) => SendCommand(ClickToMoveType.Stop, pos, guid, turnSpeed, distance);
        
        public static void Attack(Class.Vector3 pos, ulong guid, float turnSpeed = 6.28f, float distance = 3.0f) => SendCommand(ClickToMoveType.AttackGuid, pos, guid, turnSpeed, distance);

        private static void SendCommand(ClickToMoveType commandType, Class.Vector3 pos, ulong guid, float turnSpeed, float distance)
        {
            Write(OffsetManager.OffsetList.ClickToMoveX, pos.X);
            Write(OffsetManager.OffsetList.ClickToMoveY, pos.Y);
            Write(OffsetManager.OffsetList.ClickToMoveZ, pos.Z);
            Write(OffsetManager.OffsetList.ClickToMoveTurnSpeed, turnSpeed);
            Write(OffsetManager.OffsetList.ClickToMoveDistance, distance);
            Write(OffsetManager.OffsetList.ClickToMoveGuid, guid);
            Write(OffsetManager.OffsetList.ClickToMoveAction, (int)commandType);
        }
        #endregion

        public static bool Write<T>(IntPtr address, T value, int size = 0)
        {
            if (size == 0)
            {
                size = SizeOf(typeof(T));
            }

            IntPtr writeBuffer = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(value, writeBuffer, false);

            bool result = WriteProcessMemory(XMemory.Instance.Process.Handle, address, writeBuffer, size, out _);

            Marshal.DestroyStructure(writeBuffer, typeof(T));
            Marshal.FreeHGlobal(writeBuffer);

            return result;
        }

        private static int SizeOf(Type type)
        {
            DynamicMethod dm = new DynamicMethod("SizeOf", typeof(int), new Type[] { });
            ILGenerator il = dm.GetILGenerator();

            il.Emit(OpCodes.Sizeof, type);
            il.Emit(OpCodes.Ret);

            int size = (int)dm.Invoke(null, null);
            return size;
        }
    }
}
