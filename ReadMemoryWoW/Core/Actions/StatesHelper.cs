﻿using System.Collections.Generic;
using ReadMemoryWoW.Managers.GameData;
using ReadMemoryWoW.Managers.GameData.Enums;
using ReadMemoryWoW.Managers.GameData.Objects;

namespace ReadMemoryWoW.Core.Actions
{
    public static class StatesHelper
    {
        public static Unit FindNearestToPlayer(Player player)
        {
            var objMgr = ObjectManager.Instance;
            var unitsList = new List<Unit>();
            objMgr.WowObjects.ForEach(x =>
            {
                if (x.Type == WowObjectType.Unit && !((Unit)x).IsDead)
                    unitsList.Add((Unit)x);
            });

            float minDist = -1;
            Unit nearest = null;

            foreach (var x in unitsList)
            {
                var dist = x.Position.GetDistance(player.Position);
                if ((minDist < 0f || dist < minDist) && x.Health > 0 && dist < 100f)
                {
                    minDist = (float)dist;
                    nearest = x;
                }
            }

            return nearest;
        }
    }
}