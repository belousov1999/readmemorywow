﻿using ReadMemoryWoW.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadMemoryWoW.Core.EventsStruct
{
    public class EventStateChanged : EventArgs
    {
        public StatesEnum ePrevState { get; private set; }

        public StatesEnum eCurrentState { get; private set; }
    }
}
