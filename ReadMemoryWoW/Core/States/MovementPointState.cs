﻿using ReadMemoryWoW.Class;
using ReadMemoryWoW.Core.Enums;
using ReadMemoryWoW.Managers.GameData;
using System;

namespace ReadMemoryWoW.Core.States
{
    public class MovementPointState : State
    {
        public override StatesEnum eState { get; } = StatesEnum.MovingToPoint;

        private static Vector3 StartPos;

        public override void Start()
        {
            Console.WriteLine("MovementPoint start");
            var objMgr = ObjectManager.Instance;
            StartPos = objMgr.Player.Position;

            StateManager.ExecuteState(StatesEnum.MovingToTarget);
        }

        public override void Update()
        {
           
        }
    }
}
