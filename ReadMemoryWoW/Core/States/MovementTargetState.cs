﻿using ReadMemoryWoW.Core.Enums;
using ReadMemoryWoW.Managers.GameData;
using ReadMemoryWoW.Managers.GameData.Enums;
using ReadMemoryWoW.Managers.GameData.Objects;
using ReadMemoryWoW.Managers.GameData.Utils;
using System;
using System.Collections.Generic;
using ReadMemoryWoW.Core.Actions;

namespace ReadMemoryWoW.Core.States
{
    public class MovementTargetState : State
    {
        public override StatesEnum eState { get; } = StatesEnum.MovingToPoint;

        private Unit _currentTarget = null;
        private bool _isMovingForward = false;

        public override void Start()
        {
        }

        private void MainMoving(Player player)
        {
            if (_currentTarget.Health <= 0)
            {
                _currentTarget = null;
                return;
            }
            MovingOptions.Stop(player.Position, _currentTarget.Guid);

            var subState = MovementHelper.MovingToPoint(player, _currentTarget, ref _isMovingForward);
            switch (subState)
            {
                case SubMovementState.None:
                case SubMovementState.InProgress:
                    return;
                case SubMovementState.Reached:
                    StateManager.ExecuteState(StatesEnum.AttackingTarget);
                    // StateManager.ExecuteState(StatesEnum.MovingToPoint);
                    return;
                case SubMovementState.TooFar:
                    StateManager.ExecuteState(StatesEnum.MovingToPoint);
                    return;
            }
        }

        public override void Update()
        {
            var player = ObjectManager.Instance.Player;
            if (_currentTarget == null)
            {
                var nearest = StatesHelper.FindNearestToPlayer(player);
                if (nearest == null)
                {
                    Console.WriteLine("No Mobs!");
                    return;
                }
                _currentTarget = nearest;
            }

            MainMoving(player);
            Console.WriteLine("\n");
        }
    }
}
