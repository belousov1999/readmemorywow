﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReadMemoryWoW.Core.Enums;

namespace ReadMemoryWoW.Core.States
{
    public class State : ICoreState
    {
        public virtual StatesEnum eState { get; } = StatesEnum.None;

        public virtual void Start()
        {
            return;
        }

        public virtual void Update()
        {
            return;
        }
    }
}
