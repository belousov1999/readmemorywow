﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ReadMemoryWoW.Class;
using ReadMemoryWoW.Core.Actions;
using ReadMemoryWoW.Core.Enums;
using ReadMemoryWoW.Managers.GameData;
using ReadMemoryWoW.Managers.GameData.Enums;
using ReadMemoryWoW.Managers.GameData.Objects;
using ReadMemoryWoW.Managers.Server;
using ReadMemoryWoW.MemoryUtil;
using ReadMemoryWoW.Offsets;
using ReadMemoryWoW.Utils;

namespace ReadMemoryWoW.Core.States
{
    public class AttackingTargetState : State
    {
        public override StatesEnum eState { get; } = StatesEnum.AttackingTarget;

        private Unit _currentTarget = null;
        private bool _lootedLastTarget = false;


        public override void Start()
        {
        }

        void AttackingTarget()
        {
            //var pos = Camera.World2Screen.WorldToScreen(_currentTarget.Position.X, _currentTarget.Position.Y, _currentTarget.Position.Z);
            var pos = Mouse.GetCursorPosition();
            Console.WriteLine(pos.X+" "+pos.Y);
            MovingOptions.Attack(_currentTarget.Position, _currentTarget.Guid);
            if (ObjectManager.Instance.TargetGuid == 0 || (_currentTarget.IsDead && !_lootedLastTarget))
            {
                Mouse.SetCursorPosition((int)pos.X, (int)pos.Y);
                Thread.Sleep(1000);

                if (XMemory.Instance.Read(OffsetManager.OffsetList.CursorType, out int cursorType) && cursorType == 8)
                {
                    Mouse.RightClick();
                    _lootedLastTarget = true;
                    var cam = Camera.World2Screen.GetCameraMatrix();
                    var camPos = new Vector3(Camera.X, Camera.Y, Camera.Z);
                    
                    ServerBotData.SendClickInfo(new Vector2(pos.X, pos.Y), new Class.Vector2(1024, 768), _currentTarget.Position, camPos, cam);
                    _currentTarget = null;
                }
            }
        }

        public override void Update()
        {
            var player = ObjectManager.Instance.Player;
            if (_currentTarget == null)
            {
                var nearest = StatesHelper.FindNearestToPlayer(player);
                if (nearest == null)
                {
                    Console.WriteLine("No Mobs!");
                    return;
                }
                _lootedLastTarget = false;
                _currentTarget = nearest;
            }

            AttackingTarget();
        }
    }
}
