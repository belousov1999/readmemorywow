﻿using System.Collections.Generic;
using Newtonsoft.Json;
using ReadMemoryWoW.Class;

namespace ReadMemoryWoW.Core
{
    public static class StatesData
    {
        public static List<PositionPoint> PointsList { get; private set; } = new List<PositionPoint>();
        public static bool IsPointsInited { get; private set; } = false;

        public static void InitPoints(string data)
        {
            if (IsPointsInited) 
                return;

            IsPointsInited = true;
            PointsList = JsonConvert.DeserializeObject<List<PositionPoint>>(data);
        }
    }
}