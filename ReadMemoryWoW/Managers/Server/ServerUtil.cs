﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Specialized;
using System.Net;
using System.Text;

namespace ReadMemoryWoW.Managers.Server
{
    public static class ServerUtil
    {
        private const string ADRESS = @"http://localhost:4297";

        public const string EMPTY_JSON = "{}";

        public static JObject ServerRequest(string botID, string name, string data, string path)
        {
            try
            {
                using (var wb = new WebClient())
                {
                    var json = new NameValueCollection();
                    json["botID"] = botID;
                    json["name"] = name;
                    json["msg"] = data;
                    json["path"] = path;

                    var response = wb.UploadValues(ADRESS, "POST", json);
                    string responseInString = Encoding.UTF8.GetString(response);
                    return JObject.Parse(responseInString);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return JObject.Parse(@"{
                      Error: 'Request error-> steamid: " + botID + " name:" + name + @"'
                }");
            }
        }
    }
}
