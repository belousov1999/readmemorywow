﻿using Newtonsoft.Json;
using ReadMemoryWoW.Class;
using ReadMemoryWoW.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ReadMemoryWoW.Managers.Server
{
    public static class ServerBotData
    {
        public static string BotID { get; private set; } = "0000";
        public const string PATH = "_BotData";

        public static void Init()
        {
            BotID = GetValidBotID();
        }

        public static void HeartBeat() =>
            ServerUtil.ServerRequest(BotID, "HeartBeat", ServerUtil.EMPTY_JSON, PATH);
        //if we found good position of enemy on screen, send info
        public static void SendClickInfo(Vector2 screenPoint, Vector2 windowSize, Vector3 targetPos, Vector3 cameraPos, Matrix cameraMatrix)
        {
           
            var posData = new Dictionary<string, float>();
            posData.Add("screenPointX", screenPoint.X);
            posData.Add("screenPointY", screenPoint.Y);
            posData.Add("windowSizeX", windowSize.X);
            posData.Add("windowSizeY", windowSize.Y);
            posData.Add("targetPosX", (float)Math.Floor(targetPos.X));
            posData.Add("targetPosY", (float)Math.Floor(targetPos.Y));
            posData.Add("targetPosZ", (float)Math.Floor(targetPos.Z));
            posData.Add("cameraPosX", (float)Math.Floor(cameraPos.X));
            posData.Add("cameraPosY", (float)Math.Floor(cameraPos.Y));
            posData.Add("cameraPosZ", (float)Math.Floor(cameraPos.Z));
            posData.Add("cameraMatrixX1", cameraMatrix._x1);
            posData.Add("cameraMatrixX2", cameraMatrix._x2);
            posData.Add("cameraMatrixX3", cameraMatrix._x3);
            posData.Add("cameraMatrixY1", cameraMatrix._y1);
            posData.Add("cameraMatrixY2", cameraMatrix._y2);
            posData.Add("cameraMatrixY3", cameraMatrix._y3);
            posData.Add("cameraMatrixZ1", cameraMatrix._z1);
            posData.Add("cameraMatrixZ2", cameraMatrix._z2);
            posData.Add("cameraMatrixZ3", cameraMatrix._z3);

            ServerUtil.ServerRequest(BotID, "SendClickData", JsonConvert.SerializeObject(posData), PATH);
        }

        private static string GetValidBotID()
        {
            try
            {
                var h = ServerUtil.ServerRequest("0000", "GetValidBotID", "{}", PATH);
                //var succ = h.Value<bool>("Success");
                return h.Value<string>("BotID");
            }
            catch (Exception e)
            {
                Console.WriteLine("ServerRequestError: GetValidBotID ->" + e);
            }
            return "0000";
        }
    }
}
