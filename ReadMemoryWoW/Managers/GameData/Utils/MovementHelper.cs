﻿using ReadMemoryWoW.Managers.GameData.Enums;
using ReadMemoryWoW.Managers.GameData.Objects;
using ReadMemoryWoW.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReadMemoryWoW.Core.Actions;

namespace ReadMemoryWoW.Managers.GameData.Utils
{
    public static class MovementHelper
    {
        public const float REACH_DISTANCE = 25f, MAX_DISTANCE = 80f;

        public static SubMovementState MovingToPoint(Player player, Unit target, ref bool isMovingForward)
        {
            var dist = target.Position.GetDistance(player.Position);
            Console.WriteLine("[MovingToPoint]Target:" + target + "  Dist:" + dist);
            if (dist > REACH_DISTANCE && dist < MAX_DISTANCE)
            {
                var isFacing = DummyMath.IsFacing(player, target.Position);
                //Console.WriteLine(isFacing + " " + player.Rotation + " " + DummyMath.GetFacingAngle(player, target.Position));
                if (!isFacing)
                {
                    if (isMovingForward)
                        MovingOptions.StopSendingForward();
                    MovingOptions.SendLeft();
                }
                else if (!isMovingForward)
                {
                    isMovingForward = true;
                    MovingOptions.StartSendingForward();
                }
                return SubMovementState.InProgress;
            }
            else if (dist >= MAX_DISTANCE)
                return SubMovementState.TooFar;

            if (isMovingForward)
                MovingOptions.StopSendingForward();
            return SubMovementState.Reached;
        }
    }
}
