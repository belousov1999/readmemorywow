﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadMemoryWoW.Managers.GameData.Storage
{
    public interface IAmeisenBotCache
    {
        void CacheName(ulong guid, string name);

        void Clear();

        void Load();

        void Save();

        bool TryGetName(ulong guid, out string name);
    }
}
