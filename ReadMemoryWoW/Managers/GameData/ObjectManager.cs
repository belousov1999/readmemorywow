﻿using ReadMemoryWoW.Class;
using ReadMemoryWoW.Managers.GameData.Enums;
using ReadMemoryWoW.Managers.GameData.Objects;
using ReadMemoryWoW.Managers.GameData.Storage;
using ReadMemoryWoW.MemoryUtil;
using ReadMemoryWoW.Offsets;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace ReadMemoryWoW.Managers.GameData
{
    public class ObjectManager
    {
        private readonly object queryLock = new object();

        private List<SimpleObject> wowObjects;

        public ObjectManager(XMemory xMemory, IAmeisenBotCache botCache)
        {
            if (Instance != null)
                return;
            Instance = this;
            IsWorldLoaded = true;

            WowObjects = new List<SimpleObject>();
            XMemory = xMemory;
            OffsetList = OffsetManager.OffsetList;
            BotCache = botCache;
        }

        public delegate void ObjectUpdateComplete(List<SimpleObject> wowObjects);

        public event ObjectUpdateComplete OnObjectUpdateComplete;

        public bool IsWorldLoaded { get; private set; }

        public ulong LastTargetGuid { get; private set; }

        public int MapId { get; private set; }

        public Player Player { get; private set; }

        public Unit Target { get; private set; }

        public Unit LastTarget { get; private set; }

        public IntPtr PlayerBase { get; private set; }

        public ulong PlayerGuid { get; private set; }

        public ulong TargetGuid { get; private set; }

        public List<SimpleObject> WowObjects
        {
            get
            {
                lock (queryLock)
                {
                    return wowObjects;
                }
            }

            set
            {
                lock (queryLock)
                {
                    wowObjects = value;
                }
            }
        }

        public int ZoneId { get; private set; }

        private IAmeisenBotCache BotCache { get; }

        private IOffsetList OffsetList { get; }

        private XMemory XMemory { get; }

        public static ObjectManager Instance { get; private set; } = null;

        public T GetWowObjectByGuid<T>(ulong guid) where T : SimpleObject
            => WowObjects.OfType<T>().FirstOrDefault(e => e.Guid == guid);

        public SimpleObject ReadWowObject(IntPtr activeObject, WowObjectType wowObjectType = WowObjectType.None)
        {
            if (XMemory.Read(IntPtr.Add(activeObject, OffsetList.WowObjectDescriptor.ToInt32()), out IntPtr descriptorAddress)
                && XMemory.Read(IntPtr.Add(activeObject, OffsetList.WowObjectGuid.ToInt32()), out ulong guid)
                && XMemory.ReadStruct(IntPtr.Add(activeObject, OffsetList.WowObjectPosition.ToInt32()), out Vector3 wowPosition))
            {
                return new SimpleObject()
                {
                    BaseAddress = activeObject,
                    DescriptorAddress = descriptorAddress,
                    Guid = guid,
                    Type = wowObjectType,
                    Position = wowPosition
                };
            }

            return null;
        }

        public Player ReadWowPlayer(IntPtr activeObject, WowObjectType wowObjectType = WowObjectType.Player)
        {
            Unit Unit = ReadWowUnit(activeObject, wowObjectType);

            if (Unit != null)
            {
                Player player = new Player()
                {
                    BaseAddress = activeObject,
                    DescriptorAddress = Unit.DescriptorAddress,
                    Guid = Unit.Guid,
                    Type = wowObjectType,
                    Name = ReadPlayerName(Unit.Guid),
                    TargetGuid = Unit.TargetGuid,
                    Position = Unit.Position,
                    Rotation = Unit.Rotation,
                    BitUnitFlags = Unit.BitUnitFlags,
                    BitUnitFlagsDynamic = Unit.BitUnitFlagsDynamic,
                    Health = Unit.Health,
                    MaxHealth = Unit.MaxHealth,
                    Mana = Unit.Mana,
                    MaxMana = Unit.MaxMana,
                    Energy = Unit.Energy,
                    MaxEnergy = Unit.MaxEnergy,
                    Rage = Unit.Rage,
                    MaxRage = Unit.MaxRage,
                    Runeenergy = Unit.Runeenergy,
                    MaxRuneenergy = Unit.MaxRuneenergy,
                    Level = Unit.Level,
                    Class = Unit.Class,
                    PowerType = Unit.PowerType,
                    IsAutoAttacking = Unit.IsAutoAttacking,
                    CurrentlyCastingSpellId = Unit.CurrentlyCastingSpellId,
                    CurrentlyChannelingSpellId = Unit.CurrentlyChannelingSpellId
                };

                if (PlayerGuid != 0 && Unit.Guid == PlayerGuid)
                {
                    if (XMemory.Read(IntPtr.Add(Unit.DescriptorAddress, OffsetList.DescriptorExp.ToInt32()), out int exp)
                        && XMemory.Read(IntPtr.Add(Unit.DescriptorAddress, OffsetList.DescriptorMaxExp.ToInt32()), out int maxExp)
                        && XMemory.Read(OffsetList.ComboPoints, out byte comboPoints))
                    {
                        player.Exp = exp;
                        player.MaxExp = maxExp;
                        player.ComboPoints = comboPoints;
                    }

                    Player = player;
                }

                return player;
            }

            return null;
        }

        public Unit ReadWowUnit(IntPtr activeObject, WowObjectType wowObjectType = WowObjectType.Unit)
        {
            SimpleObject GameObject = ReadWowObject(activeObject, wowObjectType);

            if (GameObject != null
                && XMemory.Read(IntPtr.Add(GameObject.DescriptorAddress, OffsetList.DescriptorTargetGuid.ToInt32()), out ulong targetGuid)
                && XMemory.ReadStruct(IntPtr.Add(activeObject, OffsetList.WowUnitPosition.ToInt32()), out Vector3 wowPosition)
                && XMemory.Read(IntPtr.Add(activeObject, OffsetList.PlayerRotation.ToInt32()), out float rotation)
                && XMemory.Read(IntPtr.Add(activeObject, OffsetList.IsAutoAttacking.ToInt32()), out int isAutoAttacking)
                && XMemory.Read(IntPtr.Add(GameObject.DescriptorAddress, OffsetList.DescriptorUnitFlags.ToInt32()), out BitVector32 unitFlags)
                && XMemory.Read(IntPtr.Add(GameObject.DescriptorAddress, OffsetList.DescriptorUnitFlagsDynamic.ToInt32()), out BitVector32 dynamicUnitFlags)
                && XMemory.Read(IntPtr.Add(GameObject.DescriptorAddress, OffsetList.DescriptorHealth.ToInt32()), out int health)
                && XMemory.Read(IntPtr.Add(GameObject.DescriptorAddress, OffsetList.DescriptorMaxHealth.ToInt32()), out int maxHealth)
                && XMemory.Read(IntPtr.Add(GameObject.DescriptorAddress, OffsetList.DescriptorMana.ToInt32()), out int mana)
                && XMemory.Read(IntPtr.Add(GameObject.DescriptorAddress, OffsetList.DescriptorMaxMana.ToInt32()), out int maxMana)
                && XMemory.Read(IntPtr.Add(GameObject.DescriptorAddress, OffsetList.DescriptorRage.ToInt32()), out int rage)
                && XMemory.Read(IntPtr.Add(GameObject.DescriptorAddress, OffsetList.DescriptorMaxRage.ToInt32()), out int maxRage)
                && XMemory.Read(IntPtr.Add(GameObject.DescriptorAddress, OffsetList.DescriptorEnergy.ToInt32()), out int energy)
                && XMemory.Read(IntPtr.Add(GameObject.DescriptorAddress, OffsetList.DescriptorMaxEnergy.ToInt32()), out int maxEnergy)
                && XMemory.Read(IntPtr.Add(GameObject.DescriptorAddress, OffsetList.DescriptorRuneenergy.ToInt32()), out int runeenergy)
                && XMemory.Read(IntPtr.Add(GameObject.DescriptorAddress, OffsetList.DescriptorMaxRuneenergy.ToInt32()), out int maxRuneenergy)
                && XMemory.Read(IntPtr.Add(GameObject.DescriptorAddress, OffsetList.DescriptorLevel.ToInt32()), out int level)
                && XMemory.Read(IntPtr.Add(GameObject.DescriptorAddress, OffsetList.DescriptorInfoFlags.ToInt32()), out int infoFlags)
                && XMemory.Read(IntPtr.Add(activeObject, OffsetList.CurrentlyCastingSpellId.ToInt32()), out int currentlyCastingSpellId)
                && XMemory.Read(IntPtr.Add(activeObject, OffsetList.CurrentlyChannelingSpellId.ToInt32()), out int currentlyChannelingSpellId))
            {
                return new Unit()
                {
                    BaseAddress = activeObject,
                    DescriptorAddress = GameObject.DescriptorAddress,
                    Guid = GameObject.Guid,
                    Type = wowObjectType,
                    Name = ReadUnitName(activeObject, GameObject.Guid),
                    TargetGuid = targetGuid,
                    Position = wowPosition,
                    Rotation = rotation,
                    BitUnitFlags = unitFlags,
                    BitUnitFlagsDynamic = dynamicUnitFlags,
                    Health = health,
                    MaxHealth = maxHealth,
                    Mana = mana,
                    MaxMana = maxMana,
                    Energy = energy,
                    MaxEnergy = maxEnergy,
                    Rage = rage / 10,
                    MaxRage = maxRage / 10,
                    Runeenergy = runeenergy / 10,
                    MaxRuneenergy = maxRuneenergy / 10,
                    Level = level,
                    Class = Enum.IsDefined(typeof(WowClass), (WowClass)(infoFlags >> 8 & 0xFF)) ? (WowClass)(infoFlags >> 8 & 0xFF) : WowClass.Unknown,
                    PowerType = Enum.IsDefined(typeof(WowPowertype), (WowPowertype)(infoFlags >> 24 & 0xFF)) ? (WowPowertype)(infoFlags >> 24 & 0xFF) : WowPowertype.Unknown,
                    IsAutoAttacking = isAutoAttacking == 1,
                    CurrentlyCastingSpellId = currentlyCastingSpellId,
                    CurrentlyChannelingSpellId = currentlyChannelingSpellId
                };
            }

            return null;
        }

        public bool RefreshIsWorldLoaded()
        {
            if (XMemory.Read(OffsetList.IsWorldLoaded, out int isWorldLoaded))
            {
                IsWorldLoaded = isWorldLoaded == 1;
                return IsWorldLoaded;
            }

            return false;
        }

        public void UpdateObject<T>(T GameObject) where T : SimpleObject
            => UpdateObject(GameObject.Type, GameObject.BaseAddress);

        public void UpdateObject(WowObjectType wowObjectType, IntPtr baseAddress)
        {
            WowObjects.RemoveAll(e => e.BaseAddress == baseAddress);
            switch (wowObjectType)
            {
                case WowObjectType.Gameobject:
                    WowObjects.Add(ReadWowGameobject(baseAddress, wowObjectType));
                    break;

                case WowObjectType.Unit:
                    WowObjects.Add(ReadWowUnit(baseAddress, wowObjectType));
                    break;

                case WowObjectType.Player:
                    WowObjects.Add(ReadWowPlayer(baseAddress, wowObjectType));
                    break;

                default:
                    WowObjects.Add(ReadWowObject(baseAddress, wowObjectType));
                    break;
            }
        }

        public void UpdateWowObjects()
        {
            if (!XMemory.Read(OffsetList.IsWorldLoaded, out int isWorldLoaded))
            {
                IsWorldLoaded = isWorldLoaded == 1;
                return;
            }

            if (XMemory.Read(OffsetList.PlayerGuid, out ulong playerGuid))
            {
                PlayerGuid = playerGuid;
            }

            if (XMemory.Read(OffsetList.TargetGuid, out ulong targetGuid))
            {
                TargetGuid = targetGuid;
            }

            if (XMemory.Read(OffsetList.TargetGuid, out ulong lastTargetGuid))
            {
                LastTargetGuid = lastTargetGuid;
            }

            if (XMemory.Read(OffsetList.TargetGuid, out IntPtr playerbase))
            {
                PlayerBase = playerbase;
            }

            WowObjects = new List<SimpleObject>();
            XMemory.Read(OffsetList.ClientConnection, out IntPtr clientConnection);
            XMemory.Read(IntPtr.Add(clientConnection, OffsetList.CurrentObjectManager.ToInt32()), out IntPtr currentObjectManager);

            XMemory.Read(IntPtr.Add(currentObjectManager, OffsetList.FirstObject.ToInt32()), out IntPtr activeObject);
            XMemory.Read(IntPtr.Add(activeObject, OffsetList.WowObjectType.ToInt32()), out int objectType);

            while (isWorldLoaded == 1 && objectType <= 7 && objectType > 0)
            {
                WowObjectType wowObjectType = (WowObjectType)objectType;
                SimpleObject obj = wowObjectType switch
                {
                    WowObjectType.Gameobject => ReadWowGameobject(activeObject, wowObjectType),
                    WowObjectType.Unit => ReadWowUnit(activeObject, wowObjectType),
                    WowObjectType.Player => ReadWowPlayer(activeObject, wowObjectType),
                    _ => ReadWowObject(activeObject, wowObjectType),
                };

                WowObjects.Add(obj);

                if (obj.Guid == TargetGuid)
                {
                    Target = (Unit)obj;
                }


                if (obj.Guid == LastTargetGuid)
                {
                    LastTarget = (Unit)obj;
                }

                XMemory.Read(IntPtr.Add(activeObject, OffsetList.NextObject.ToInt32()), out activeObject);
                XMemory.Read(IntPtr.Add(activeObject, OffsetList.WowObjectType.ToInt32()), out objectType);
            }


            if (XMemory.Read(OffsetList.MapId, out int mapId))
            {
                MapId = mapId;
            }

            if (XMemory.Read(OffsetList.ZoneId, out int zoneId))
            {
                ZoneId = zoneId;
            }

            OnObjectUpdateComplete?.Invoke(WowObjects);
        }

        private string ReadPlayerName(ulong guid)
        {
            if (BotCache.TryGetName(guid, out string cachedName))
            {
                return cachedName;
            }

            uint shortGuid;
            uint offset;

            XMemory.Read(IntPtr.Add(OffsetList.NameStore, OffsetList.NameMask.ToInt32()), out uint playerMask);
            XMemory.Read(IntPtr.Add(OffsetList.NameStore, OffsetList.NameBase.ToInt32()), out uint playerBase);

            shortGuid = (uint)guid & 0xfffffff;
            offset = 12 * (playerMask & shortGuid);

            XMemory.Read(new IntPtr(playerBase + offset + 8), out uint current);
            XMemory.Read(new IntPtr(playerBase + offset), out offset);

            if ((current & 0x1) == 0x1)
            {
                return string.Empty;
            }

            XMemory.Read(new IntPtr(current), out uint testGuid);

            while (testGuid != shortGuid)
            {
                XMemory.Read(new IntPtr(current + offset + 4), out current);

                if ((current & 0x1) == 0x1)
                {
                    return string.Empty;
                }

                XMemory.Read(new IntPtr(current), out testGuid);
            }

            XMemory.ReadString(IntPtr.Add(new IntPtr(current), OffsetList.NameString.ToInt32()), Encoding.UTF8, out string name, 32);

            if (name.Length > 0)
            {
                BotCache.CacheName(guid, name);
            }

            return name;
        }

        private string ReadUnitName(IntPtr activeObject, ulong guid)
        {
            if (BotCache.TryGetName(guid, out string cachedName))
            {
                return cachedName;
            }

            try
            {
                XMemory.Read(IntPtr.Add(activeObject, 0x964), out uint objName);
                XMemory.Read(IntPtr.Add(new IntPtr(objName), 0x05C), out objName);

                XMemory.ReadString(new IntPtr(objName), Encoding.UTF8, out string name, 128);

                if (name.Length > 0)
                {
                    BotCache.CacheName(guid, name);
                }

                return name;
            }
            catch
            {
                return "unknown";
            }
        }

        private GameObject ReadWowGameobject(IntPtr activeObject, WowObjectType wowObjectType)
        {
            SimpleObject GameObject = ReadWowObject(activeObject, wowObjectType);

            if (GameObject != null
                && XMemory.Read(IntPtr.Add(GameObject.DescriptorAddress, OffsetList.WowGameobjectType.ToInt32()), out WowGameobjectType gameobjectType)
                && XMemory.Read(IntPtr.Add(GameObject.DescriptorAddress, OffsetList.WowGameobjectLevel.ToInt32()), out int level)
                && XMemory.Read(IntPtr.Add(GameObject.DescriptorAddress, OffsetList.WowGameobjectDisplayId.ToInt32()), out int displayId)
                && XMemory.ReadStruct(IntPtr.Add(activeObject, OffsetList.WowGameobjectPosition.ToInt32()), out Vector3 wowPosition))
            {
                return new GameObject()
                {
                    BaseAddress = activeObject,
                    DescriptorAddress = GameObject.DescriptorAddress,
                    Guid = GameObject.Guid,
                    Position = wowPosition,
                    Type = wowObjectType,
                    DisplayId = displayId,
                    Level = level,
                    GameobjectType = gameobjectType,
                };
            }

            return null;
        }
    }
}
