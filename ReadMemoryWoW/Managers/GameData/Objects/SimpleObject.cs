﻿using ReadMemoryWoW.Class;
using ReadMemoryWoW.Managers.GameData.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadMemoryWoW.Managers.GameData.Objects
{
    public class SimpleObject
    {
        public IntPtr BaseAddress { get; set; }

        public IntPtr DescriptorAddress { get; set; }

        public ulong Guid { get; set; }

        public Vector3 Position { get; set; }

        public WowObjectType Type { get; set; }
    }
}
