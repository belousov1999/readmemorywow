﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadMemoryWoW.Managers.GameData.Objects
{
    public class Player : Unit
    {
        public int Exp { get; set; }

        public int MaxExp { get; set; }

        public int ComboPoints { get; set; }
    }
}
