﻿using ReadMemoryWoW.Managers.GameData.Enums;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadMemoryWoW.Managers.GameData.Objects
{
    public class Unit : GameObject
    {
        public WowClass Class { get; set; }

        public int CurrentlyCastingSpellId { get; set; }

        public int CurrentlyChannelingSpellId { get; set; }

        public int Energy { get; set; }

        public double EnergyPercentage => ReturnPercentage(Energy, MaxEnergy);

       // public int FactionTemplate { get; set; }

        public int Health { get; set; }

        public double HealthPercentage => ReturnPercentage(Health, MaxHealth);

        public bool IsAutoAttacking { get; set; }

        public bool IsConfused => BitUnitFlags[(int)UnitFlags.Confused];

        public bool IsDazed => BitUnitFlags[(int)UnitFlags.Dazed];

        public bool IsDead => Health == 0 || BitUnitFlagsDynamic[(int)UnitDynamicFlags.Dead];

        public bool IsDisarmed => BitUnitFlags[(int)UnitFlags.Disarmed];

        public bool IsFleeing => BitUnitFlags[(int)UnitFlags.Fleeing];

        public bool IsInCombat => BitUnitFlags[(int)UnitFlags.Combat];

        public bool IsInFlightmasterFlight => BitUnitFlags[(int)UnitFlags.FlightmasterFlight];

        public bool IsLootable => BitUnitFlagsDynamic[(int)UnitDynamicFlags.Lootable];

        public bool IsLooting => BitUnitFlags[(int)UnitFlags.Looting];

        public bool IsMounted => BitUnitFlags[(int)UnitFlags.Mounted];

        public bool IsNotAttackable => BitUnitFlags[(int)UnitFlags.NotAttackable];

        public bool IsPetInCombat => BitUnitFlags[(int)UnitFlags.PetInCombat];

        public bool IsPvpFlagged => BitUnitFlags[(int)UnitFlags.PvpFlagged];

        public bool IsReferAFriendLinked => BitUnitFlagsDynamic[(int)UnitDynamicFlags.ReferAFriendLinked];

        public bool IsSilenced => BitUnitFlags[(int)UnitFlags.Silenced];

        public bool IsSitting => BitUnitFlags[(int)UnitFlags.Sitting];

        public bool IsSkinnable => BitUnitFlags[(int)UnitFlags.Skinnable];

        public bool IsSpecialInfo => BitUnitFlagsDynamic[(int)UnitDynamicFlags.SpecialInfo];

        public bool IsTaggedByMe => BitUnitFlagsDynamic[(int)UnitDynamicFlags.TaggedByMe];

        public bool IsTaggedByOther => BitUnitFlagsDynamic[(int)UnitDynamicFlags.TaggedByOther];

        public bool IsTappedByThreat => BitUnitFlagsDynamic[(int)UnitDynamicFlags.TappedByThreat];

        public bool IsTotem => BitUnitFlags[(int)UnitFlags.Totem];

        public bool IsTrackedUnit => BitUnitFlagsDynamic[(int)UnitDynamicFlags.TrackUnit];

        public int Level { get; set; }

        public int Mana { get; set; }

        public double ManaPercentage => ReturnPercentage(Mana, MaxMana);

        public int MaxEnergy { get; set; }

        public int MaxHealth { get; set; }

        public int MaxMana { get; set; }

        public int MaxRage { get; set; }

        public int MaxRuneenergy { get; set; }

        public string Name { get; set; }

        public WowPowertype PowerType { get; set; }

        public int Rage { get; set; }

        public double RagePercentage => ReturnPercentage(Rage, MaxRage);

        public float Rotation { get; set; }

        public int Runeenergy { get; set; }

        public double RuneenergyPercentage => ReturnPercentage(Runeenergy, MaxRuneenergy);

        public ulong TargetGuid { get; set; }

        public BitVector32 BitUnitFlags { get; set; }

        public BitVector32 BitUnitFlagsDynamic { get; set; }

        private double ReturnPercentage(int value, int max)
        {
            if (value == 0 || max == 0)
            {
                return 0;
            }
            else
            {
                return (double)value / (double)max * 100.0;
            }
        }

        public override string ToString()
        {
            return "[Unit] " + Name + " " + Position;
        }
    }
}
