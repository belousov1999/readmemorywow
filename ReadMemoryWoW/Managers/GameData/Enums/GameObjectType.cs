﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadMemoryWoW.Managers.GameData.Enums
{
    public enum GameObjectType
    {
        None = 0,
        Item = 1,
        Container = 2,
        Unit = 3,
        Player = 4,
        Gameobject = 5,
        Dynobject = 6,
        Corpse = 7
    }
}
