﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadMemoryWoW.Managers.GameData.Enums
{
    public enum SubMovementState
    {
        None = -1,
        InProgress = 1,
        Reached = 2,
        TooFar = 3
    }
}
