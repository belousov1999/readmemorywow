﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadMemoryWoW.Managers.GameData.Enums
{
    public enum WowGameobjectType
    {
        Door,
        Button,
        Questgiver,
        Chest,
        Binder,
        Generic,
        Trap,
        Chair,
        Focus,
        Text,
        Goober,
        Transport,
        Areadamage,
        Camera,
        Mapobject,
        Motransport,
        Duelflag,
        Fishingnode,
        Summoningritual,
        Mailbox,
        Empty,
        Guardpost,
        Spellcaster,
        Meetingstone,
        Flagstand,
        Fishinghole,
        Flagdrop,
        Minigame,
        Empty2,
        Capturepoint,
        Auragenerator,
        Dungeondifficulty,
        Barberchair,
        Destructiblebuilding,
        Guildbank,
        Trapdoor
    }
}
