﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadMemoryWoW.Managers.GameData.Enums
{
    public enum WowPowertype
    {
        Unknown = -1,
        Mana = 0,
        Rage = 1,
        Energy = 3,
        Runes = 5,
        Runeenergy = 6
    }
}
